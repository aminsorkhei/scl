\contentsline {section}{\numberline {1}Introduction}{2}
\contentsline {section}{\numberline {2}Data Collection and Processing}{2}
\contentsline {section}{\numberline {3}Classifiers}{3}
\contentsline {subsection}{\numberline {3.1}Naive Bayes Classifier}{3}
\contentsline {subsection}{\numberline {3.2}Decision Tree}{4}
\contentsline {subsection}{\numberline {3.3}K-Nearest Neighbors}{5}
\contentsline {subsection}{\numberline {3.4}Classification Instructions}{6}
\contentsline {subsection}{\numberline {3.5}Code Example}{7}
\contentsline {section}{\numberline {4}Results}{7}
\contentsline {section}{\numberline {5}Discussion and Future Work}{12}
