__author__ = 'sorkhei'

from bs4 import BeautifulSoup
import csv, codecs, cStringIO, requests, json
from fake_useragent import UserAgent
from requests.packages.urllib3.exceptions import InsecureRequestWarning

import pandas as pd
from sklearn.naive_bayes import GaussianNB, BernoulliNB
from sklearn import tree
from sklearn.neighbors import KNeighborsClassifier
import joblib

requests.packages.urllib3.disable_warnings(InsecureRequestWarning)


class UnicodeWriter:
    """
    A CSV writer which will write rows to CSV file "f",
    which is encoded in the given encoding.
    """

    def __init__(self, f, dialect=csv.excel, encoding="utf-8", **kwds):
        # Redirect output to a queue
        self.queue = cStringIO.StringIO()
        self.writer = csv.writer(self.queue, dialect=dialect, **kwds)
        self.stream = f
        self.encoder = codecs.getincrementalencoder(encoding)()

    def writerow(self, row):
        self.writer.writerow([s.encode("utf-8") for s in row])
        # Fetch UTF-8 output from the queue ...
        data = self.queue.getvalue()
        data = data.decode("utf-8")
        # ... and reencode it into the target encoding
        data = self.encoder.encode(data)
        # write to the target stream
        self.stream.write(data)
        # empty queue
        self.queue.truncate(0)

    def writerows(self, rows):
        for row in rows:
            self.writerow(row)


def collectData(web_address, output_file_name, country_code):
    '''
    :param web_address(str): address used for webscraping and collecting data
    :param output_file_name(str): path to the intended output file
    :param country_code(str): the basis country to collect the data based on
    :return: This function does not return any specific output but it generates datasets as csv files
    '''

    print 'Please WAIT, fetching data from "%s" and storing in "%s.csv" ' % (country_code, output_file_name)

    # Set up CSV
    csv_writer = UnicodeWriter(open(output_file_name + '.csv', 'w'))
    csv_writer.writerow(['sep=,'])
    csv_writer.writerow(['type', 'title', 'genre', 'ranking', 'ranking trend', 'ranking diff (ovr. month)', 'rating'
        , 'price', 'iap', 'publisher', 'url'])

    # set up fake request to fake a browser
    fake_header = {'User-Agent': UserAgent().random}
    html_code = requests.get(web_address, headers=fake_header).content
    soup = BeautifulSoup(html_code)

    all_games = soup.findAll("div", {'class': 'item'})
    for game in all_games:

        game_name = game.find('span', {'class': 'product-name'}).text
        game_icon_url = game.find('span', {'class': 'icon'}).find('img').get('src')
        game_type = game.parent.parent.find('h3').contents[0]
        game_iap = '1' if game.find('span', {'class': 'iap'}) is not None else '0'
        game_ranking = game.find('span', {'class': 'number'}).contents[0]
        game_publisher = game.find('span', {'class': 'publisher-name'}).contents[0]
        price_check = game.find('span', {'class': 'price'}).contents[0]
        game_price = price_check.replace('$', '') if price_check != 'FREE' else '0'

        # Use game_package_name fot further lookup
        game_package_name = game.a.get('href').split('/')[4]

        # Create query to access appTweak.io
        appTweak_header = {'X-Apptweak-Key': 'WbqbC18vybX3M-gD8_Z9XZpK7h8'}
        query = 'https://api.apptweak.com/android/applications/%s.json?country=%s' % (game_package_name, country_code)

        game_json = json.loads(requests.get(query, headers=appTweak_header, verify=False).content)

        # sanity check for rating
        if not game_json['content']['ratings']:
            game_rating = '?'
        else:
            game_rating = '%.1f' % game_json['content']['ratings']['average']

        # ranking over the past month or earliest available data in the specified country
        # sanity check for ranking
        if not game_json['content']['rankings']:
            game_ranking_difference = game_ranking_trend = '0'
        else:
            game_ranking_list = filter(lambda ranking: ranking is not None,
                                       game_json['content']['rankings'][0]['ranks'])
            game_ranking_difference = game_ranking_list[0] - game_ranking_list[-1]
            game_ranking_trend = '-1' if game_ranking_difference < 0 else '+1' if game_ranking_difference > 0 else '0'

        # Just care about the most important genre
        game_genres = game_json['content']['store_info']['genres'][0]

        # write the obtained result in a CSV file
        csv_writer.writerow(
            [game_type, game_name, game_genres, game_ranking, game_ranking_trend, str(game_ranking_difference),
             game_rating, game_price, game_iap,
             game_publisher, game_icon_url])
    print 'Data is ready'


def data_processing(training_csv_file, test_csv_file):
    '''
    :param training_csv_file: directory to the csv file containing training set
    :param test_csv_file: directory to the csv file containing test set
    :return: This function does not return any specific output but it creates trainingData.dta,
    testData.dta and y_train.dta which are read to be fed to a classifier
    '''
    # we only use test data for sanity check and NOT during training
    training_data = pd.read_csv(training_csv_file, skiprows=1)
    test_data = pd.read_csv(test_csv_file, skiprows=1)
    # Based on the instructions, we are not allowed to use the group as a predictive feature, thus we ignore the price
    # feature as well
    training_data = training_data[
        ['type', 'genre', 'ranking', 'ranking trend', 'ranking diff (ovr. month)', 'rating', 'iap']]
    test_data = test_data[training_data.columns]
    # concat data for pre-processing purposes
    data = training_data.append(test_data)

    # 1.discretize rankings by intervals [1-10][11-20][21-30] and so on
    data.ranking = pd.cut(data.ranking, [0, 10, 20, 30, 40, 51])
    # 2.discretize ranking difference
    data['ranking diff (ovr. month)'] = pd.qcut(data['ranking diff (ovr. month)'], q=10)
    # 3.discretize ratings
    data.rating = pd.qcut(data.rating, q=5)
    # 4.binerize the categorical variables
    data = pd.get_dummies(data, columns=['genre', 'ranking', 'ranking diff (ovr. month)', 'rating'])
    # get back to training and test data
    training_data = data[0:150]
    test_data = data[150:200]
    y_train = pd.DataFrame(-1, index=pd.np.arange(training_data.shape[0]), columns=['label'])
    y_train.label[training_data[training_data.type == 'GROSSING'].index.tolist()[0:20]] = 1

    # get rid of unwanted columns
    training_data.pop('type')
    test_data.pop('type')
    # For sanity check and preserving order
    test_data = test_data[training_data.columns]
    joblib.dump(training_data, 'trainingData.dta')
    joblib.dump(test_data, 'testData.dta')
    joblib.dump(y_train, 'y_train.dta')


def training(training_data, y_train, clf_type):
    '''
    :param training_data(str): directory a .dta file generated by data_processing method containing training dataset
    :param y_train(str): directory a .dta file generated by data_processing method containing label for training dataset
    :param clf_type(str): a string which determines the type of the intended classifier. This can be dt for decision
    tree, gnb for gaussian naive bayes, bnb for bernouli naive bayes and knn for k-nearest neighbours
    :return: this function does not return any specific output but it generates a classifier and stores that as a .clf
    file
    '''
    # initialize the classifier
    training_data = joblib.load(training_data)
    y_train = joblib.load(y_train)

    if clf_type == 'dt':
        clf = tree.DecisionTreeClassifier(random_state=0, max_depth=4)
    elif clf_type == 'gnb':
        clf = GaussianNB()
    elif clf_type == 'bnb':
        clf = BernoulliNB()
    elif clf_type == 'knn':
        clf = KNeighborsClassifier(n_neighbors=8)

    clf.fit(training_data, y_train)
    joblib.dump(clf, '%s.clf' % clf_type)


def testing(test_data, clf_type):
    '''
    :param: test_data(str): directory to a test set stored as a .dta file generated by processing method.
    :param clf_type(str): a string which determines the type of the intended classifier. This can be dt for decision
    tree, gnb for gaussian naive bayes, bnb for bernouli naive bayes and knn for k-nearest neighbours
    :return: this function does not return any specific output but it stores the classification result as a .csv file
    '''
    test_data = joblib.load(test_data)
    clf = joblib.load('%s.clf' % clf_type)
    result = pd.DataFrame(data=clf.predict_proba(test_data)
                          , columns=['NotGrossing probability', 'Grossing Probability'])
    result.to_csv('%s.csv' % clf_type)
    print 'Classification result has been saved to "%s.csv". for more info please refer to the report.' % clf_type


'''
au_address = 'https://www.appannie.com/apps/google-play/top/australia/game/'
ch_address = 'https://www.appannie.com/apps/google-play/top/china/game/'
collectData(web_address=au_address, outpu_file_name='games', country_code='au')
collectData(web_address=ch_address, outpu_file_name='test', country_code='cn')
data_processing(training_csv_file='games.csv', test_csv_file='test.csv')
training(training_data='trainingData.dta', y_train='y_train.dta', clf_type='dt')
testing(test_data='testData.dta', clf_type='dt')
'''
